<?php

class MY_Controller extends CI_Controller{

	public function __construct(){
		parent::__construct();

        date_default_timezone_set('Asia/Jakarta');
        ini_set("date.timezone", "Asia/Jakarta");

		$this->load->library('session');
        $this->load->helper('url');


		if(!$this->session->userdata()){
			redirect(base_url("login"));
		} else {
			// $this->check_session();
		}
	}
	private function check_session($user_name = null){
		$userdata = $this->session->userdata('user_data');
		if($user_name){
			if($user_name != $userdata['user_name']){
				redirect('logout');
			}
		}
        $last_time = $userdata['last_time'];
        $curr_time = time();
        $mins = ($curr_time - $last_time) / 60;
        if ($mins > 20) {
            // redirect('logouts');
        } else {
            $userdata['last_time'] = $curr_time;
            $this->session->set_userdata('userdata', $userdata);
        }
	}
    public function is_logged_in(){
        $user = $this->session->userdata('user_data');
        return isset($user);
    }
    public function get_modul_task($modul,$modul_task){
    	$this->load->model('Coa_model');
        $modul_result = $this->Coa_model->get_coa_jurnal($modul,urldecode($modul_task));
        if(isset($modul_result['id_coa'])){
            $moduls = array(
                'coa_id' => $modul_result['id_coa'],
                'coa_kode' => $modul_result['kode_coa'],
                'coa_nama' => $modul_result['nama_coa'],
                'modul' => $modul_result['modul'],
                'modul_task' => $modul_result['modul_task'],
                'type' => $modul_result['type']
            );
            $result = array('status' => 1,
                'message' => 'Success', 'modul' => $modul, 'modul_task' => urldecode($modul_task),
                'data' => $moduls
            );
        }else{
            $result = array('status' => 0,
                'message' => 'COA Jurnal Not Found', 'modul' => $modul, 'modul_task' => urldecode($modul_task),
                'data' => ''
            );
        }
        // $this->output->set_content_type('application/json');
        // $this->output->set_output(json_encode($result));
        return $result;
    }
    public function get_modul_task_json($modul,$modul_task){
        $this->load->model('Coa_model');
        $modul_result = $this->Coa_model->get_coa_jurnal($modul,urldecode($modul_task));
        if(isset($modul_result['id_coa'])){
            $moduls = array(
                'coa_id' => $modul_result['id_coa'],
                'coa_kode' => $modul_result['kode_coa'],
                'coa_nama' => $modul_result['nama_coa'],
                'modul' => $modul_result['modul'],
                'modul_task' => $modul_result['modul_task'],
                'type' => $modul_result['type']
            );
            $result = array('status' => 1,
                'message' => 'Success', 'modul' => $modul, 'modul_task' => urldecode($modul_task),
                'data' => $moduls
            );
        }else{
            $result = array('status' => 0,
                'message' => 'COA Jurnal Not Found', 'modul' => $modul, 'modul_task' => urldecode($modul_task),
                'data' => ''
            );
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($result));
        // return $result;
    }
    public function time_ago($timestamp){
        $selisih = time() - strtotime($timestamp) ;
        $detik = $selisih ;
        $menit = round($selisih / 60 );
        $jam = round($selisih / 3600 );
        $hari = round($selisih / 86400 );
        $minggu = round($selisih / 604800 );
        $bulan = round($selisih / 2419200 );
        $tahun = round($selisih / 29030400 );

        if ($detik <= 60) {
            $waktu = $detik.' detik yang lalu';
        } else if ($menit <= 60) {
            $waktu = $menit.' menit yang lalu';
        } else if ($jam <= 24) {
            $waktu = $jam.' jam yang lalu';
        } else if ($hari <= 7) {
            $waktu = $hari.' hari yang lalu';
        } else if ($minggu <= 4) {
            $waktu = $minggu.' minggu yang lalu';
        } else if ($bulan <= 12) {
            $waktu = $bulan.' bulan yang lalu';
        } else {
            $waktu = $tahun.' tahun yang lalu';
        }
        return $waktu;
    }
    public function say_number($x){ #1200 -> seribu dua ratus
        $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        if ($x < 12){
            return " " . $abil[$x];
        }
        elseif ($x < 20){
            return $this->say_number($x - 10) . " belas";
        }
        elseif ($x < 100){
            return $this->say_number($x / 10) . " puluh" . $this->say_number($x % 10);
        }
        elseif ($x < 200){
            return " seratus" . $this->say_number($x - 100);
        }
        elseif ($x < 1000){
            return $this->say_number($x / 100) . " ratus" . $this->say_number($x % 100);
        }
        elseif ($x < 2000){
            return " seribu" . $this->say_number($x - 1000);
        }
        elseif ($x < 1000000){
            return $this->say_number($x / 1000) . " ribu" . $this->say_number($x % 1000);
        }
        elseif ($x < 1000000000){
            return $this->say_number($x / 1000000) . " juta" . $this->say_number($x % 1000000);
        }
        elseif ($x < 1000000000000){
            return $this->say_number($x / 1000000000) . " milyar" . $this->say_number($x % 1000000000);
        }
        elseif ($x < 1000000000000000){
            return $this->say_number($x / 1000000000000) . " milyar" . $this->say_number($x % 1000000000000);
        }
    }
    public function random_code($length){ # JEH3F2
        $text = 'ABCDEFGHJKLMNOPQRSTUVWXYZ23456789';
        $txtlen = strlen($text)-1;
        $result = '';
        for($i=1; $i<=$length; $i++){
        $result .= $text[rand(0, $txtlen)];}
        return $result;
    }
    public function random_number($length){ # JEH3F2
        $text = '1234567890';
        $txtlen = strlen($text)-1;
        $result = '';
        for($i=1; $i<=$length; $i++){
        $result .= $text[rand(0, $txtlen)];}
        return $result;
    }
    public function random_session($length){
        $text = 'ABCDEFGHJKLMNOPQRSTUVWXYZ'.time();
        $txtlen = strlen($text)-1;
        $result = '';
        for($i=1; $i<=$length; $i++){
        $result .= $text[mt_rand(0, $txtlen)];}
        return $result;
    }
    public function lowercase($var){ //testing
        $final=strtolower($var);
        return $final;
    }
    public function uppercase($var){ //TESTING
        $final=strtoupper($var);
        return $final;
    }
    public function sentencecase($var){ //Testing
        $final=ucfirst($var);
        return $final;
    }
    public function capitalize($var){ // Testing Bro
        $var=strtolower($var);
        $final=ucwords($var);
        return $final;
    }
    public function safe($var){
        $v = trim($var);
        $v = strip_tags($v);
        $v = htmlentities($v);
        $v = strtolower($v);
        $v = ucwords($v);
        return $v;
    }
    public function generate_seo_link($var){
        $v = trim($var);
        $v = strtolower($v);
        $v = preg_replace('/[^a-zA-Z0-9\s\-]/','',$v);
        $v = str_replace(' ','-',$v);
        return $v;
    }
    public function encrypt_decrypt($action, $string){
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'AA74CDCC2BBRT935136HH7B63C27'; // user define private key
        $secret_iv = 'm45t3r'; // user define secret key
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16); // sha256 is hash_hmac_algo
        if ($action == 'e') { //encrypt
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        }else if($action == 'd') { //decrypt
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }
    public function age($tanggal){
        list($thn,$bln,$tgl) = explode("-",$tanggal);
        $fthn = date("Y");
        $fbln = date("m");
        $ftgl = date("d");
        $pas=gregoriantojd($bln,$tgl,$thn);
        $now=gregoriantojd($fbln,$ftgl,$fthn);
        $umur=$now-$pas;
        $t=floor($umur/365);
        return $t." years old"; 
    }
    public function generate_username($string){
        $final = strtolower($string);
        $final = str_replace(' ','.',$final);
        return $final;        
    }
    public function file_upload_image($dir = "", $img_data = ""){
        $r          = new \stdClass();
        $r->status  = 0;
        $r->message = 'Failed';
        $r->result  = array();

        $dir = (substr($dir, -1) != "/" ? $dir . "/" : $dir); // konfig directory data
    
        // validate
        if (empty($dir)) {
            return "";
        }
        if (empty($img_data)) {
            return "";
        }
        if ($img_data == "undefined") {
            return "";
        }
    
        $img_arr_a = explode(";", $img_data);
        $img_arr_b = explode(",", $img_arr_a[1]);
    
        $file_data = base64_decode($img_arr_b[1]);
        $file_name = $this->random_number(20);
        $file_ext = ".png";

        //File Success Move
        if(file_put_contents($dir . $file_name . $file_ext, $file_data)){
            
            //Compress Image
            /*
                $compress['width']              = 64;
                $compress['height']             = 64;
                $compress['image_library']      = 'gd2';
                $compress['source_image']       = $dir . $file_name . $file_ext;
                $compress['create_thumb']       = false;
                $compress['maintain_ratio']     = true;
                $compress['new_image']          = FCPATH . $dir . $file_name . $file_ext;
                $this->load->library('image_lib', $compress);
                $this->image_lib->resize();
                $this->image_lib->clear();
            */

            $r->status=1;
            $r->result = array(
                'file_directory' => $dir, /* upload/product/ */
                'file_name' => $file_name, /* 1231421*/
                'file_ext' => $file_ext, /* .png */
                'file_location' => $dir . $file_name . $file_ext
            );
        }
        return $r;
    }
}

?>