<?php

class Login_model extends CI_Model
{
    
    function cek_login($table,$where)
    {
        return $this->db->get_where($table,$where);
    }

    function login_get_id($table,$where)
    {
        $operator = $this->db->get_where($table,$where)->row();        
        return json_decode(json_encode($operator), True);
    }    
        
    function get_login_info($user_name){
        $this->db->select('users.*');
        return $this->db->get_where('users',array('users.user_username'=>$user_name))->row_array();
    }
    function get_login_info_switch_user($user_id){
        $this->db->select('users.*');
        return $this->db->get_where('users',array('users.user_id'=>$user_id))->row_array();
    }    
    function get_group_info($id){
        return $this->db->get_where('users_groups',array('user_group_id'=>$id))->row_array();
    }      
}
