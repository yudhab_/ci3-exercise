<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends MY_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        
        if(!$this->is_logged_in()){
            redirect(base_url("login"));
        }
        $this->load->model('User_model'); 

        $this->app_name = 'CI3 Exercise';            
        $this->app_url  = site_url();  
        $this->app_logo = site_url().'upload/branch/default_logo.png';
        $this->app_logo_sidebar = site_url().'upload/branch/default_sidebar.png';        
    }

    function index(){
        $data['session'] = $this->session->userdata();
        $session = $this->session->userdata();
        $session_user_id = $session['user_data']['user_id'];
        $firstdate = new DateTime('first day of this month');
        $firstdateofmonth = $firstdate->format('d-m-Y');

        // Date Now
        $datenow =date("d-m-Y");
        $data['first_date'] = $firstdateofmonth;
        $data['end_date'] = $datenow;

        $data['branch'] = array(
            'branch_logo' => $this->app_logo,
            'branch_logo_login' => $this->app_logo,
            'branch_logo_sidebar' => $this->app_logo_sidebar          
        );
        $data['title'] = 'Dashboard';
        $this->load->view('layouts/admin/index',$data);   
    }
    function display_404(){
        $this->load->view('webarch/404');
    }
    function display_dashboard(){
        $data['session'] = $this->session->userdata();
        $this->load->view('webarch/dashboard');
    }

}

    

   