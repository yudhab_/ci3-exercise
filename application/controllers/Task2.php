<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Task2 extends MY_Controller{

    function __construct(){
        parent::__construct();
        if (!$this->is_logged_in()) {
			$this->session->set_userdata('url_before',base_url(uri_string()));
			redirect(base_url("login/return_url"));           
		}
        $this->load->model('User_model');
    }

    function index(){
        if ($this->input->post()) {   
            //Your code Here
        }else{
            $data['session'] = $this->session->userdata();  
            $session_user_id = !empty($data['session']['user_data']['user_id']) ? $data['session']['user_data']['user_id'] : null;

            $data['title'] = 'Task 2';
            $data['_view'] = 'layouts/admin/menu/task2';
            $this->load->view('layouts/admin/index',$data);
            $this->load->view('layouts/admin/menu/task2_js',$data);            
        }
    }
}
?>