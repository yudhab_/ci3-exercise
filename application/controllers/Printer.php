<?php 
// https://social.technet.microsoft.com/Forums/windows/en-US/3b85dc4f-95b0-41f3-806e-c6f0cbe963a8/print-from-network-shared-printer-using-command-prompt?forum=w7itproinstall
defined('BASEPATH') OR exit('No direct script access allowed');

class Printer extends MY_Controller{

    var $folder_upload = 'uploads/printer/';

    function __construct(){
        parent::__construct();

        if(!$this->is_logged_in()){

            //Will Return to Last URL Where session is empty
            $this->session->set_userdata('url_before',base_url(uri_string()));
            redirect(base_url("login/return_url"));
        }
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('User_model');
        $this->load->model('Printer_model');     
    }
    function index(){
        if ($this->input->post()) {    
            $return = new \stdClass();
            $return->status = 0;
            $return->message = '';
            $return->result = '';

            $upload_directory = $this->folder_upload;     
            $upload_path_directory = FCPATH . $upload_directory;

            $data['session'] = $this->session->userdata();  
            $session_user_id = !empty($data['session']['user_data']['user_id']) ? $data['session']['user_data']['user_id'] : null;

            $post = $this->input->post();
            $get  = $this->input->get();
            $action = !empty($this->input->post('action')) ? $this->input->post('action') : false;
            
            switch($action){
                case "load":
                    $columns = array(
                        '0' => 'printer_id',
                        '1' => 'printer_name'
                    );

                    $limit     = !empty($post['length']) ? $post['length'] : 10;
                    $start     = !empty($post['start']) ? $post['start'] : 0;
                    $order     = !empty($post['order']) ? $columns[$post['order'][0]['column']] : $columns[0];
                    $dir       = !empty($post['order'][0]['dir']) ? $post['order'][0]['dir'] : "asc";
                    
                    $search    = [];
                    if(!empty($post['search']['value'])) {
                        $s = $post['search']['value'];
                        foreach ($columns as $v) {
                            $search[$v] = $s;
                        }
                    }

                    $params = array(
                        'printer_type >' => 0
                    );

                    if($post['filter_flag'] !== "All") {
                        $params['printer_flag'] = $post['filter_flag'];
                    }else{
                        $params['printer_flag <'] = 4;
                    }

                    $get_data = $this->Printer_model->get_all_printer($params, $search, $limit, $start, $order, $dir);
                    $get_count = $this->Printer_model->get_all_printer_count($params);

                    if(isset($get_data)){
                        $return->total_records   = $get_count;
                        $return->status          = 1; 
                        $return->result          = $get_data;
                    }else{
                        $return->total_records   = 0;
                        $return->result          = $get_data;
                    }
                    $return->params = $params;
                    $return->message             = 'Load '.$return->total_records.' data';
                    $return->recordsTotal        = $return->total_records;
                    $return->recordsFiltered     = $return->total_records;
                    break;
                case "create":
                    // $data = base64_decode($post);
                    // $data = json_decode($post, TRUE);

                    $this->form_validation->set_rules('printer_name', 'printer_name', 'required');
                    $this->form_validation->set_message('required', '{field} wajib diisi');
                    if ($this->form_validation->run() == FALSE){
                        $return->message = validation_errors();
                    }else{

                        $printer_name = !empty($post['printer_name']) ? $post['printer_name'] : null;
                        $printer_flag = !empty($post['printer_flag']) ? $post['printer_flag'] : 0;
                        $printer_session = $this->random_code(20);

                        $params = array(
                            'printer_name' => $printer_name,
                            'printer_flag' => $printer_flag
                        );
                        $params = array(
                            // 'printer_id' => !empty($post['printer_id']) ? intval($post['printer_id']) : null,
                            // 'printer_parent_id' => !empty($post['printer_parent_id']) ? $post['printer_parent_id'] : null,
                            'printer_type' => !empty($post['printer_type']) ? intval($post['printer_type']) : null,
                            'printer_ip' => !empty($post['printer_ip']) ? $post['printer_ip'] : null,
                            'printer_name' => !empty($post['printer_name']) ? $post['printer_name'] : null,
                            'printer_flag' => !empty($post['printer_flag']) ? intval($post['printer_flag']) : 0,
                            'printer_session' => $printer_session,
                            'printer_date_created' => date("YmdHis"),
                            // 'printer_paper_design' => !empty($post['printer_paper_design']) ? $post['printer_paper_design'] : null,
                            // 'printer_paper_width' => !empty($post['printer_paper_width']) ? intval($post['printer_paper_width']) : null,
                            // 'printer_paper_height' => !empty($post['printer_paper_height']) ? intval($post['printer_paper_height']) : null,
                        );

                        //Check Data Exist
                        $params_check = array(
                            'printer_name' => $printer_name
                        );
                        $check_exists = $this->Printer_model->check_data_exist($params_check);
                        if(!$check_exists){

                            $set_data=$this->Printer_model->add_printer($params);
                            if($set_data){

                                $printer_id = $set_data;
                                $data = $this->Printer_model->get_printer($printer_id);

                                $return->status=1;
                                $return->message='Berhasil menambahkan '.$post['printer_name'];
                                $return->result= array(
                                    'id' => $set_data,
                                    'name' => $post['printer_name'],
                                    'session' => $printer_session
                                ); 
                            }else{
                                $return->message='Gagal menambahkan '.$post['printer_name'];
                            }
                        }else{
                            $return->message='Data sudah ada';
                        }
                    }
                    break;
                case "read":
                    $this->form_validation->set_rules('printer_id', 'printer_id', 'required');
                    if ($this->form_validation->run() == FALSE){
                        $return->message = validation_errors();
                    }else{                
                        $printer_id   = !empty($post['printer_id']) ? $post['printer_id'] : 0;
                        if(intval(strlen($printer_id)) > 0){        
                            $datas = $this->Printer_model->get_printer($printer_id);
                            if($datas){
                                $return->status=1;
                                $return->message='Berhasil mendapatkan data';
                                $return->result=$datas;
                            }else{
                                $return->message = 'Data tidak ditemukan';
                            }
                        }else{
                            $return->message='Data tidak ada';
                        }
                    }
                    break;
                case "update":
                    $this->form_validation->set_rules('printer_id', 'printer_id', 'required');
                    $this->form_validation->set_message('required', '{field} tidak ditemukan');
                    if ($this->form_validation->run() == FALSE){
                        $return->message = validation_errors();
                    }else{
                        $printer_id = !empty($post['printer_id']) ? $post['printer_id'] : $post['printer_id'];
                        $printer_name = !empty($post['printer_name']) ? $post['printer_name'] : $post['printer_name'];
                        $printer_flag = !empty($post['printer_flag']) ? $post['printer_flag'] : $post['printer_flag'];

                        if(strlen($printer_id) > 0){
                            $params = array(
                                // 'printer_id' => !empty($post['printer_id']) ? intval($post['printer_id']) : null,
                                // 'printer_parent_id' => !empty($post['printer_parent_id']) ? $post['printer_parent_id'] : null,
                                'printer_type' => !empty($post['printer_type']) ? intval($post['printer_type']) : null,
                                'printer_ip' => !empty($post['printer_ip']) ? $post['printer_ip'] : null,
                                'printer_name' => !empty($post['printer_name']) ? $post['printer_name'] : null,
                                'printer_flag' => !empty($post['printer_flag']) ? intval($post['printer_flag']) : 0,
                                // 'printer_date_created' => date("YmdHis"),
                                // 'printer_paper_design' => !empty($post['printer_paper_design']) ? $post['printer_paper_design'] : null,
                                // 'printer_paper_width' => !empty($post['printer_paper_width']) ? intval($post['printer_paper_width']) : null,
                                // 'printer_paper_height' => !empty($post['printer_paper_height']) ? intval($post['printer_paper_height']) : null,
                            );

                            /*
                            if(!empty($data['password'])){
                                $params['password'] = md5($data['password']);
                            }
                            */
                           
                            $set_update=$this->Printer_model->update_printer($printer_id,$params);
                            if($set_update){
                                
                                $get_data = $this->Printer_model->get_printer($printer_id);
                                $return->status  = 1;
                                $return->message = 'Berhasil memperbarui '.$printer_name;
                            }else{
                                $return->message='Gagal memperbarui '.$printer_name;
                            }   
                        }else{
                            $return->message = "Gagal memperbarui";
                        } 
                    }
                    break;
                case "delete":
                    $this->form_validation->set_rules('printer_id', 'printer_id', 'required');
                    if ($this->form_validation->run() == FALSE){
                        $return->message = validation_errors();
                    }else{
                        $printer_id   = !empty($post['printer_id']) ? $post['printer_id'] : 0;
                        $printer_name = !empty($post['printer_name']) ? $post['printer_name'] : null;

                        if(strlen($printer_id) > 0){
                            $get_data=$this->Printer_model->get_printer($printer_id);
                            // $set_data=$this->Printer_model->delete_printer($printer_id);
                            $set_data = $this->Printer_model->update_printer_custom(array('printer_id'=>$printer_id),array('printer_flag'=>4));                
                            if($set_data){
                                /*
                                if (file_exists($get_data['printer_image'])) {
                                    unlink($get_data['printer_image']);
                                } 
                                */
                                $return->status=1;
                                $return->message='Berhasil menghapus '.$printer_name;
                            }else{
                                $return->message='Gagal menghapus '.$printer_name;
                            } 
                        }else{
                            $return->message='Data tidak ditemukan';
                        }
                    }
                    break;
                case "update_flag":
                    $this->form_validation->set_rules('printer_id', 'printer_id', 'required');
                    $this->form_validation->set_rules('printer_flag', 'printer_flag', 'required');
                    $this->form_validation->set_message('required', '{field} wajib diisi');
                    if($this->form_validation->run() == FALSE){
                        $return->message = validation_errors();
                    }else{
                        $printer_id = !empty($post['printer_id']) ? $post['printer_id'] : 0;
                        if(strlen(intval($printer_id)) > 0){
                            
                            $params = array(
                                'printer_flag' => !empty($post['printer_flag']) ? intval($post['printer_flag']) : 0,
                            );
                            
                            $where = array(
                                'printer_id' => !empty($post['printer_id']) ? intval($post['printer_id']) : 0,
                            );
                            
                            if($post['printer_flag']== 0){
                                $set_msg = 'nonaktifkan';
                            }else if($post['printer_flag']== 1){
                                $set_msg = 'mengaktifkan';
                            }else if($post['printer_flag']== 4){
                                $set_msg = 'menghapus';
                            }else{
                                $set_msg = 'mendapatkan data';
                            }

                            $set_update=$this->Printer_model->update_printer_custom($where,$params);
                            if($set_update){
                                $get_data = $this->Printer_model->get_printer_custom($where);
                                $return->status  = 1;
                                $return->message = 'Berhasil '.$set_msg.' '.$get_data['printer_name'];
                            }else{
                                $return->message='Gagal '.$set_msg;
                            }   
                        }else{
                            $return->message = 'Gagal mendapatkan data';
                        } 
                    }
                    break;
                case "create_printer_item":
                    // $data = base64_decode($post);
                    // $data = json_decode($post, TRUE);

                    $this->form_validation->set_rules('printer_parent_id', 'Header Printer', 'required');
                    $this->form_validation->set_message('required', '{field} wajib diisi');
                    if ($this->form_validation->run() == FALSE){
                        $return->message = validation_errors();
                    }else{

                        $printer_item_name = !empty($post['printer_name']) ? $post['printer_name'] : null;
                        $printer_item_flag = !empty($post['printer_flag']) ? $post['printer_flag'] : 0;
                        $printer_item_session = $this->random_code(20);

                        $params = array(
                            // 'printer_id' => !empty($post['printer_id']) ? intval($post['printer_id']) : null,
                            'printer_parent_id' => !empty($post['printer_parent_id']) ? $post['printer_parent_id'] : null,
                            // 'printer_type' => !empty($post['printer_type']) ? intval($post['printer_type']) : null,
                            // 'printer_ip' => !empty($post['printer_ip']) ? $post['printer_ip'] : null,
                            'printer_name' => !empty($post['printer_paper_name']) ? $post['printer_paper_name'] : null,
                            'printer_flag' => 0,
                            'printer_session' => $printer_item_session,                            
                            'printer_date_created' => date("YmdHis"),
                            'printer_paper_design' => !empty($post['printer_paper_design']) ? $post['printer_paper_design'] : null,
                            'printer_paper_width' => !empty($post['printer_paper_width']) ? intval($post['printer_paper_width']) : null,
                            'printer_paper_height' => !empty($post['printer_paper_height']) ? intval($post['printer_paper_height']) : null,
                        );
                        //Check Data Exist
                        $params_check = array(
                            'printer_paper_width' => $post['printer_paper_width'],
                            'printer_paper_height' => $post['printer_paper_height']
                        );
                        $check_exists = $this->Printer_model->check_data_exist($params_check);
                        if(!$check_exists){

                            $set_data=$this->Printer_model->add_printer($params);
                            if($set_data){

                                $printer_item_id = $set_data;
                                $data = $this->Printer_model->get_printer($printer_item_id);
                                $return->status=1;
                                $return->message='Berhasil menambahkan';
                                $return->result= array(
                                    'id' => $set_data,
                                    'session' => $printer_item_session
                                ); 
                            }else{
                                $return->message='Gagal menambahkan';
                            }
                        }else{
                            $return->message='Data sudah ada';
                        }
                    }
                    break;
                case "read_printer_item":
                    $this->form_validation->set_rules('printer_id', 'printer_id', 'required');
                    if ($this->form_validation->run() == FALSE){
                        $return->message = validation_errors();
                    }else{                
                        $printer_item_id   = !empty($post['printer_id']) ? $post['printer_id'] : 0;
                        if(intval(strlen($printer_item_id)) > 0){        
                            $datas = $this->Printer_model->get_printer($printer_item_id);
                            if($datas){
                                $return->status=1;
                                $return->message='Berhasil mendapatkan data';
                                $return->result=$datas;
                            }else{
                                $return->message = 'Data tidak ditemukan';
                            }
                        }else{
                            $return->message='Data tidak ada';
                        }
                    }
                    break;
                case "update_printer_item":
                    $this->form_validation->set_rules('printer_id', 'printer_id', 'required');
                    $this->form_validation->set_message('required', '{field} tidak ditemukan');
                    if ($this->form_validation->run() == FALSE){
                        $return->message = validation_errors();
                    }else{
                        $printer_item_id = !empty($post['printer_id']) ? $post['printer_id'] : $post['printer_id'];

                        if(strlen($printer_item_id) > 0){
                            $params = array(
                                // 'printer_id' => !empty($post['printer_id']) ? intval($post['printer_id']) : null,
                                // 'printer_parent_id' => !empty($post['printer_parent_id']) ? $post['printer_parent_id'] : null,
                                // 'printer_type' => !empty($post['printer_type']) ? intval($post['printer_type']) : null,
                                // 'printer_ip' => !empty($post['printer_ip']) ? $post['printer_ip'] : null,
                                'printer_name' => !empty($post['printer_paper_name']) ? $post['printer_paper_name'] : null,
                                'printer_paper_design' => !empty($post['printer_paper_design']) ? $post['printer_paper_design'] : null,
                                'printer_paper_width' => !empty($post['printer_paper_width']) ? intval($post['printer_paper_width']) : null,
                                'printer_paper_height' => !empty($post['printer_paper_height']) ? intval($post['printer_paper_height']) : null,
                            );
                           
                            $set_update=$this->Printer_model->update_printer($printer_item_id,$params);
                            if($set_update){
                                $get_data = $this->Printer_model->get_printer($printer_item_id);
                                $return->status  = 1;
                                $return->message = 'Berhasil memperbarui';
                            }else{
                                $return->message='Gagal memperbarui';
                            }   
                        }else{
                            $return->message = "Gagal memperbarui";
                        } 
                    }
                    break;
                case "update_printer_item_flag":
                    $this->form_validation->set_rules('printer_id', 'printer_id', 'required');
                    $this->form_validation->set_rules('printer_flag', 'printer_flag', 'required');
                    $this->form_validation->set_message('required', '{field} wajib diisi');
                    if($this->form_validation->run() == FALSE){
                        $return->message = validation_errors();
                    }else{
                        $printer_item_id = !empty($post['printer_id']) ? $post['printer_id'] : 0;
                        if(strlen(intval($printer_item_id)) > 0){
                            
                            $params = array(
                                'printer_flag' => !empty($post['printer_flag']) ? intval($post['printer_flag']) : 0,
                            );
                            
                            $where = array(
                                'printer_id' => !empty($post['printer_id']) ? intval($post['printer_id']) : 0,
                            );
                            
                            if($post['printer_flag']== 0){
                                $set_msg = 'nonaktifkan';
                            }else if($post['printer_flag']== 1){
                                $set_msg = 'mengaktifkan';
                            }else if($post['printer_flag']== 4){
                                $set_msg = 'menghapus';
                            }else{
                                $set_msg = 'mendapatkan data';
                            }

                            $set_update=$this->Printer_model->update_printer_custom($where,$params);
                            if($set_update){
                                $get_data = $this->Printer_model->get_printer_custom($where);
                                $return->status  = 1;
                                $return->message = 'Berhasil '.$set_msg.' '.$get_data['printer_name'];
                            }else{
                                $return->message='Gagal '.$set_msg;
                            }   
                        }else{
                            $return->message = 'Gagal mendapatkan data';
                        } 
                    }
                    break;
                case "delete_printer_item":
                    $this->form_validation->set_rules('printer_id', 'printer_id', 'required');
                    if ($this->form_validation->run() == FALSE){
                        $return->message = validation_errors();
                    }else{
                        $printer_item_id   = !empty($post['printer_id']) ? $post['printer_id'] : 0;
                        $printer_item_name = !empty($post['printer_name']) ? $post['printer_name'] : null;                                

                        if(strlen($printer_item_id) > 0){
                            $get_data=$this->Printer_model->get_printer($printer_item_id);
                            $set_data=$this->Printer_model->delete_printer($printer_item_id);
                            // $set_data = $this->Printer_model->update_printer_custom(array('printer_id'=>$printer_item_id),array('printer_flag'=>4));                
                            if($set_data){
                                /*
                                if (file_exists($get_data['printer_item_image'])) {
                                    unlink($get_data['printer_item_image']);
                                } 
                                */
                                $return->status=1;
                                $return->message='Berhasil menghapus '.$printer_item_name;
                            }else{
                                $return->message='Gagal menghapus '.$printer_item_name;
                            } 
                        }else{
                            $return->message='Data tidak ditemukan';
                        }
                    }
                    break;
                case "load_printer_item":
                    $columns = array(
                        '0' => 'printer_id',
                        '1' => 'printer_name'
                    );

                    $limit     = !empty($post['length']) ? $post['length'] : 10;
                    $start     = !empty($post['start']) ? $post['start'] : 0;
                    $order     = !empty($post['order']) ? $columns[$post['order'][0]['column']] : $columns[0];
                    $dir       = !empty($post['order'][0]['dir']) ? $post['order'][0]['dir'] : "asc";
                    
                    $search    = [];
                    if(!empty($post['search']['value'])) {
                        $s = $post['search']['value'];
                        foreach ($columns as $v) {
                            $search[$v] = $s;
                        }
                    }

                    $params = array();

                    //Default Params for Master CRUD Form
                    // $params['printer_item_id']   = !empty($post['printer_item_id']) ? $post['printer_item_id'] : $params;
                    // $params['printer_item_name'] = !empty($post['printer_item_name']) ? $post['printer_item_name'] : $params;

                    /*
                    if($post['other_item_column'] && $post['other_item_column'] > 0) {
                        $params['other_item_column'] = $post['other_item_column'];
                    }
                    */
                    
                    $get_data = $this->Printer_model->get_all_printer($params, $search, $limit, $start, $order, $dir);
                    $get_count = $this->Printer_model->get_all_printer_count($params);

                    if(isset($get_data)){
                        $return->total_records   = $get_count;
                        $return->status          = 1; 
                        $return->result          = $get_data;
                    }else{
                        $return->total_records   = 0;
                        $return->result          = $get_data;
                    }
                    $return->message             = 'Load '.$return->total_records.' data';
                    $return->recordsTotal        = $return->total_records;
                    $return->recordsFiltered     = $return->total_records;
                    break;
                case "load_printer_item_2":
                    $params = array(); $total  = 0;
                    $this->form_validation->set_rules('printer_item_printer_id', 'printer_item_printer_id', 'required');
                    if ($this->form_validation->run() == FALSE){
                        $return->message = validation_errors();
                    }else{
                        $printer_item_printer_id   = !empty($post['printer_item_printer_id']) ? $post['printer_item_printer_id'] : 0;
                        if(intval(strlen($printer_item_printer_id)) > 0){
                            $params = array(
                                'printer_parent_id' => $printer_item_printer_id
                            );
                            $search = null;
                            $start  = null;
                            $limit  = null;
                            $order  = "printer_id";
                            $dir    = "asc";
                            $get_data = $this->Printer_model->get_all_printer($params, $search, $limit, $start, $order, $dir);
                            if($get_data){
                                $total = count($get_data);
                                $return->status=1;
                                $return->message='Berhasil mendapatkan data';
                                $return->result=$get_data;
                            }else{
                                $return->message = 'Data tidak ditemukan';
                            }
                        }else{
                            $return->message='Data tidak ada';
                        }
                    }
                    $return->params          =$params;
                    $return->total_records   = $total;
                    $return->recordsTotal    = $total;
                    $return->recordsFiltered = $total;
                    break;
                default:
                    $return->message='No Action';
                    break; 
            }
            echo json_encode($return);
        }else{
            // Default First Date & End Date of Current Month
            $firstdate = new DateTime('first day of this month');
            $firstdateofmonth = $firstdate->format('d-m-Y');

            $data['session'] = $this->session->userdata();  
            $session_user_id = !empty($data['session']['user_data']['user_id']) ? $data['session']['user_data']['user_id'] : null;

            $data['first_date'] = $firstdateofmonth;
            $data['end_date'] = date("d-m-Y");
            $data['hour'] = date("H:i");

            $data['title'] = 'Printer';
            $data['_view'] = 'layouts/admin/menu/printer';
            $this->load->view('layouts/admin/index',$data);
            $this->load->view('layouts/admin/menu/printer_js.php',$data);
        }
    }
}

?>