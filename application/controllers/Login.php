<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends My_Controller{
    var $folder_location = array(
        '0' => array(
            'title' => 'Login',
            'view' => 'layouts/admin/login/login',  
        )
    );
    function __construct(){
        parent::__construct();
        $this->load->helper('date');       
        $this->load->helper('form');
        $this->load->helper('cookie');        
        $this->load->helper('url');
        
        $this->load->library('form_validation');
        $this->load->library('user_agent');

        $this->load->model('Login_model');         
        $this->load->model('User_model');    

        //Get Branch
        $this->app_name     = 'Ci Exercise';            
        $this->app_url      = site_url();  
        $this->package_id   = 3;
        $this->package_name = 'Enterprise';      
        $this->app_logo     = site_url().'upload/branch/default_logo.png';
        $this->app_logo_sidebar = site_url().'upload/branch/default_sidebar.png';                                
    }
    function index(){ //Default Login Index Layout
        $data['branch'] = array(
            'branch_logo' => $this->app_logo,
            'branch_logo_login' => $this->app_logo,
            'branch_logo_sidebar' => $this->app_logo_sidebar          
        );            
        $data['title'] = $this->folder_location['0']['title']; //Login
        return $this->load->view($this->folder_location['0']['view'],$data); 
    }
    function authentication(){ //Post From Login
        $return = new \stdClass();
        $return->status = 0;
        $return->message = '';
        $return->result = '';
        
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_message('required', '{field} wajib diisi');
        if ($this->form_validation->run() == FALSE){
            $return->message = validation_errors();
        }else{
            $url_before = $this->input->post('url');
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            
            $where = array(
                'user_password' => md5($password),
                'user_flag' => 1
            );

            //Username / Email / Phone (Login)
            $where['user_username'] = $username;
            // $where['user_email_1'] = $username;
            // $where['user_phone_1'] = $username;
            // var_dump($where);die;
            
            $cek = $this->Login_model->cek_login("users",$where)->num_rows();
            if($cek > 0){
                $user_info = $this->Login_model->get_login_info($username);
                $update_user_last_login = $this->User_model->update_user($user_info['user_id'],array('user_date_last_login'=>date("YmdHis")));
                $user_group = $this->Login_model->get_group_info($user_info['user_user_group_id']); 
                
                
                //Session Directory
                $session_directory = site_url('admin');
                $session_array=array(
                    'user_directory' => $session_directory,
                    'user_id' => $user_info['user_id'],
                    'user_session' => $user_info['user_session'],
                    'user_name' => $username,
                    'user_fullname' => $user_info['user_fullname'],
                    'user_phone' => $user_info['user_phone_1'],
                    'user_email' => $user_info['user_email_1'],       
                    'user_group' => !empty($user_group['user_group_name']) ? $user_group['user_group_name'] : '-',
                    'user_group_id' => !empty($user_group['user_group_id']) ? intval($user_group['user_group_id']) : 0,
                    'user_type' => $user_info['user_type'],
                    'user_code' => $user_info['user_code'],                
                    'user_activation_code' => $user_info['user_activation_code'],                
                    'last_time' => date('Ymdhis'),
                    'branch' => array(
                        'id' => 1,                                       
                    )
                );
                
                $set_user_name = $user_info['user_username'];
                $set_user_mail = $user_info['user_email_1'];

                //Set Cookie
                $cookie = array(
                    'name' => site_url(),
                    'value' => $username,
                    'expire' => strtotime('+3 day'),
                    'path' => '/'                    
                );
                $this->input->set_cookie($cookie);

                $this->session->set_userdata('logged_in',true);            
                $this->session->set_userdata('user_data',$session_array);
                if($user_info['user_id'] == 1){
                    $this->session->set_userdata('root',true);                                    
                }else{
                    $this->session->set_userdata('root',false);                                                            
                }
                $this->session->set_userdata('menu_display',intval($user_info['user_menu_style'])); 

                if(!empty($url_before)){
                    $return_url = $url_before;
                    $login_message = 'Akses Diberikan';
                }else{
                    $return_url = $session_directory;
                    $login_message = 'Akses Diterima';
                }
                
                $return->status=1;
                $return->message = $login_message;
                $return->result = array(
                    'session' => $session_array,
                    'return_url' => $return_url
                );
            }else{
                $return->message = 'Wrong User Pass or Nonactive User';
            }
        }
        echo json_encode($return);       
    }
    function logout(){
        $this->session->sess_destroy();
        redirect('login');
    }
    function return_url(){
        return $this->load->view('layouts/admin/login/login');        
    }
    function remove_cookie(){
        $data['session'] = $this->session->userdata();             
        $this->session->sess_destroy();

        $cookie_name   = site_url();
        $get_cookie    = get_cookie($cookie_name);
        $delete_cookie = delete_cookie(site_url());
        if($delete_cookie){ 
            redirect('login'); 
        }else{
            redirect('login'); 
        }    
    }
    function session(){
        $data['session'] = $this->session->userdata();
        // if(!$this->is_logged_in()){
            // redirect(base_url("login"));
        // }else{
            echo json_encode(array('session'=>$data['session']));
        // }
    }
    function user_agent(){
        if ($this->agent->is_browser()){
            $agent = $this->agent->browser(); 
            /* '.$this->agent->version(); */
        } elseif ($this->agent->is_robot()){
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()){
            $agent = $this->agent->mobile();
        } else{
            $agent = 'Unidentified User Agent';
        }
        // return array('ip' => $this->input->ip_address(),'browser' => $agent,'os' => $this->agent->platform());
        return $agent.', '.$this->agent->platform();
    }
    function random_code($length){
        $text = 'ABCDEFGHJKLMNOPQRSTUVWXYZ23456789';
        $txtlen = strlen($text)-1;
        $result = '';
        for($i=1; $i<=$length; $i++){
        $result .= $text[rand(0, $txtlen)];}
        return $result;
    }
}
