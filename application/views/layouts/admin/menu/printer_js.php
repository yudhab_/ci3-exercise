<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    let identity = 0;
    let url = "<?= base_url('printer'); ?>";
    let url_print = "<?= base_url('printer'); ?>";
    let url_tool = "<?= base_url('search/manage'); ?>";

    $(function() {
        // setInterval(function(){ 
        //     //SummerNote
        //     $('#printer_note').summernote({
        //         placeholder: 'Tulis keterangan disini!',
        //         tabsize: 4,
        //         height: 200
        //     });  
        // }, 3000);
    });

    $('.upload1-link').magnificPopup({
        type: 'image',
        mainClass: 'mfp-with-zoom', // this class is for CSS animation below
            zoom: {
                enabled: true, // By default it's false, so don't forget to enable it
                duration: 300, // duration of the effect, in milliseconds
                easing: 'ease-in-out', // CSS transition easing function
                // The "opener" function should return the element from which popup will be zoomed in
                // and to which popup will be scaled down
                // By defailt it looks for an image tag:
                opener: function (openerElement) {
                    // openerElement is the element on which popup was initialized, in this case its <a> tag
                    // you don't need to add "opener" option if this code matches your needs, it's defailt one.
                    return openerElement.is('img') ? openerElement : openerElement.find('img');
                }
            }
    });
    //Select2
    /*
    $('#select').select2({
        //dropdownParent:$("#modal-id"), //If Select2 Inside Modal
        //placeholder: '<i class="fas fa-search"></i> Search',
        //width:'100%',
        placeholder: {
            id: '0',
            text: '-- Pilih --'
        },
        minimumInputLength: 0,
        allowClear: true,
        ajax: {
            type: "get",
            url: url_tool,
            dataType: 'json',
            delay: 250,
            data: function (params) {
                var query = {
                    search: params.term,
                    action:'search',
                    type: 1,
                    source: 'select_source'
                }
                return query;
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
        cache: true
        },
        escapeMarkup: function(markup){ 
            return markup; 
        },
        templateResult: function(datas){ //When Select on Click
            if (!datas.id) { return datas.text; }
            if($.isNumeric(datas.id) == true){
                // return '<i class="fas fa-user-check '+datas.id.toLowerCase()+'"></i> '+datas.text;
                return datas.text;
            }else{
                // return '<i class="fas fa-plus '+datas.id.toLowerCase()+'"></i> '+datas.text;
                return datas.text;
            }
        },
        templateSelection: function(datas) { //When Option on Click
            if (!datas.id) { return datas.text; }
            //Custom Data Attribute
            $(datas.element).attr('data-alamat', datas.alamat);
            return datas.text;
        }
    });
    $("#select").on('change', function(e){
        // Do Something
    });
    */
    // $("select").select2();

    //Date Clock Picker
    $("#printer_date").datepicker({
        // defaultDate: new Date(),
        format: 'dd-mm-yyyy',
        autoclose: true,
        enableOnReadOnly: true,
        language: "id",
        todayHighlight: true,
        weekStart: 1 
    }).on('change', function(e){
    });
    $('.clockpicker').clockpicker({
        default: 'now',
        placement: 'bottom',
        align: 'left',
        donetext: 'Done',
        autoclose: true
    }).on('change', function(e){
    });
    $("#filter_start_date, #filter_end_date").datepicker({
        // defaultDate: new Date(),
        format: 'dd-mm-yyyy',
        autoclose: true,
        enableOnReadOnly: true,
        language: "id",
        todayHighlight: true,
        weekStart: 1 
    }).on('change', function(e){
        e.stopImmediatePropagation();
        printer_table.ajax.reload();
    });

    //Autonumeric
    const autoNumericOption = {
        digitGroupSeparator : ',', 
        decimalCharacter  : '.',
        decimalCharacterAlternative: '.', 
        decimalPlaces: 0,
        watchExternalChanges: true
    };
    // new AutoNumeric('#some_id', autoNumericOption);
    

    //Datatable
    let printer_table = $("#table-data").DataTable({
        "serverSide": true,
        "ajax": {
            url: url,
            type: 'post',
            dataType: 'json',
            cache: 'false',
            data: function(d) {
                d.action = 'load';
                d.length = $("#filter_length").find(':selected').val();
                d.date_start = $("#filter_start_date").val();
                d.date_end = $("#filter_end_date").val();
                d.filter_flag = $("#filter_flag").find(':selected').val();
                d.search = {value:$("#filter_search").val()};
            },
            dataSrc: function(data) {
                return data.result;
            }
        },
        "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
        "columnDefs": [
            {"targets":0, "width":"30%", "title":"Nama Printer", "searchable":true, "orderable":true},
            {"targets":1, "width":"20%", "title":"Tipe", "searchable":true, "orderable":true},
            {"targets":2, "width":"20%", "title":"Status", "searchable":false, "orderable":true},            
            {"targets":3, "width":"20%", "title":"Action", "searchable":true, "orderable":true},
        ],
        "order": [[0, 'ASC']],
        "columns": [
            {
                'data': 'printer_id',
                className: 'text-left',
                render: function(data, meta, row) {
                    var dsp = '';
                    dsp += row.printer_name;
                    return dsp;
                }
            },{
                'data': 'printer_id',
                className: 'text-left',
                render: function(data, meta, row) {
                    var dsp = '';
                    if(row.printer_type == 1){
                        dsp += 'Deskjet';
                    }else if(row.printer_type == 2){
                        dsp += 'Dot Matrik';
                    }else if(row.printer_type == 3){
                        dsp += 'Label Works';
                    }else if(row.printer_type == 4){
                        dsp += 'Receipt Thermal';
                    }else{
                        dsp += '-';
                    }
                    return dsp;
                }
            },{
                'data': 'printer_id',
                className: 'text-left',
                render: function(data, meta, row) {
                    var dsp = '';
                    if(row.printer_flag == 1){
                        dsp += 'Aktif';
                    }else if(row.printer_flag == 4){
                        dsp += 'Terhapus';
                    }else{
                        dsp += 'Nonaktif';
                    }
                    return dsp;
                }
            },{                
                'data': 'printer_id',
                className: 'text-left',
                render: function(data, meta, row) {
                    var dsp = ''; var label = 'Error Status'; var icon = 'fas fa-cog'; var color = 'white'; var bgcolor = '#d1dade';
                    if(parseInt(row.printer_flag) == 1){
                    //  dsp += '<label style="color:#6273df;">Aktif</label>';
                       label = 'Aktif';
                       icon = 'fas fa-lock';
                       bgcolor = '#0aa699';
                    }else if(parseInt(row.printer_flag) == 4){
                       //  dsp += '<label style="color:#ff194f;">Terhapus</label>';
                       label = 'Terhapus';
                       icon = 'fas fa-trash';
                       bgcolor = '#f35958';
                    }else if(parseInt(row.printer_flag) == 0){
                       //   dsp += '<label style="color:#ff9019;">Nonaktif</label>';
                       label = 'Nonaktif';
                       icon = 'fas fa-unlock';
                       // color = 'green';
                       bgcolor = '#ff9019';
                    }

                       /* Button Action Concept 2 */
                       dsp += '&nbsp;<div class="btn-group">';
                       // dsp += '    <button class="btn btn-mini btn-default"><span class="fas fa-cog"></span></button>';
                       dsp += '    <button class="btn btn-mini btn-default dropdown-toggle btn-demo-space" data-toggle="dropdown" aria-expanded="true"><span class="fas fa-cog"></span><span class="caret"></span> </button>';
                       dsp += '    <ul class="dropdown-menu">';
                       dsp += '        <li>';
                       dsp += '            <a class="btn_edit_printer" style="cursor:pointer;"';
                       dsp += '                data-printer-id="'+data+'" data-printer-name="'+row.printer_name+'" data-printer-flag="'+row.printer_flag+'" data-printer-session="'+row.printer_session+'">';
                       dsp += '                <span class="fas fa-edit"></span> Edit';
                       dsp += '            </a>';
                       dsp += '        </li>';
                       // if(parseInt(row.printer_flag) === 0) {
                               dsp += '<li>'; 
                               dsp += '    <a class="btn_update_flag_printer" style="cursor:pointer;"';
                               dsp += '        data-printer-id="'+data+'" data-printer-name="'+row.printer_name+'" data-printer-flag="'+row.printer_flag+'" data-printer-session="'+row.printer_session+'">';
                               dsp += '        <span class="fas fa-lock"></span> Aktifkan';
                               dsp += '    </a>';
                               dsp += '</li>';
                       // }else if(parseInt(row.printer_flag) === 1){
                               dsp += '<li>';
                               dsp += '    <a class="btn_update_flag_printer" style="cursor:pointer;"';
                               dsp += '        data-printer-id="'+data+'" data-printer-name="'+row.printer_name+'" data-printer-flag="'+row.printer_flag+'" data-printer-session="'+row.printer_session+'">';
                               dsp += '        <span class="fas fa-ban"></span> Nonaktifkan';
                               dsp += '    </a>';
                               dsp += '</li>';
                       // }
                       if((parseInt(row.printer_flag) < 1) || (parseInt(row.printer_flag) == 4)) {
                               dsp += '<li>';
                               dsp += '    <a class="btn_update_flag_printer" style="cursor:pointer;"';
                               dsp += '        data-printer-id="'+data+'" data-printer-name="'+row.printer_name+'" data-printer-flag="4" data-printer-session="'+row.printer_session+'">';
                               dsp += '        <span class="fas fa-trash"></span> Hapus';
                               dsp += '    </a>';
                               dsp += '</li>';
                       }
                       dsp += '    </ul>';
                       dsp += '</div>';

                       /* Button Action Concept 2 */
                    //    dsp += '&nbsp;<div class="btn-group">';
                    //    // dsp += '    <button class="btn btn-mini btn-default" style="background-color:'+bgcolor+';color:'+color+'"><span class="'+icon+'"></span> '+label+'</button>';
                    //    dsp += '    <button class="btn btn-mini btn-default dropdown-toggle btn-demo-space" style="background-color:'+bgcolor+';color:'+color+';" data-toggle="dropdown" aria-expanded="true"><span class="'+icon+'"></span> '+label+' <span class="caret" style="color:'+color+'"></span> </button>';
                    //    dsp += '    <ul class="dropdown-menu">';
                    //    if(parseInt(row.printer_flag) == 1){
                    //            dsp += '<li>';
                    //            dsp += '    <a class="btn_update_flag_printer" style="cursor:pointer;"';
                    //            dsp += '        data-printer-id="'+data+'" data-printer-name="'+row.printer_name+'" data-printer-flag="'+row.printer_flag+'" data-printer-session="'+row.printer_session+'">';
                    //            dsp += '        <span class="fas fa-ban"></span> Nonaktifkan';
                    //            dsp += '    </a>';
                    //            dsp += '</li>';
                    //    }else if(parseInt(row.printer_flag) == 0) {
                    //            dsp += '<li>'; 
                    //            dsp += '    <a class="btn_update_flag_printer" style="cursor:pointer;"';
                    //            dsp += '        data-printer-id="'+data+'" data-printer-name="'+row.printer_name+'" data-printer-flag="'+row.printer_flag+'" data-printer-session="'+row.printer_session+'">';
                    //            dsp += '        <span class="fas fa-lock"></span> Aktifkan';
                    //            dsp += '    </a>';
                    //            dsp += '</li>';
                    //    }
                    //    dsp += '    </ul>';
                    //    dsp += '</div>';
                    return dsp;
                }
            }
        ],
        "language": {
            "sProcessing":    "Sedang memuat",
            "sLengthMenu":    "Tampil _MENU_ data",
            "sZeroRecords":   "Data tidak ada",
            "sEmptyTable":    "Data tidak ada",
            "sInfo":          "Data _START_ sampai _END_ dari _TOTAL_ data",
            "sInfoEmpty":     "Data 0 sampai 0 dari 0 data",
            "sInfoFiltered":  "(saring total _MAX_ data)",
            "sInfoPostFix":   "",
            "sSearch":        "Cari:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Loading...",
            "oPaginate": {
                "sFirst":    "Pertama",
                "sLast":    "Terakhir",
                "sNext":    "Selanjutnya",
                "sPrevious": "Sebelumnya"
            },
            "oAria": {
                "sSortAscending":  ": Mengurutkan A-Z / 0-9",
                "sSortDescending": ": Mengurutkan Z-A / 9-0"
            }
        }
    });
    $("#table-data_filter").css('display','none');
    $("#table-data_length").css('display','none');
    $("#filter_length").on('change', function(e){
        var value = $(this).find(':selected').val(); 
        $('select[name="table-data_length"]').val(value).trigger('change');
        printer_table.ajax.reload();
    });
    $("#filter_flag").on('change', function(e){ printer_table.ajax.reload(); });
    $("#filter_search").on('input', function(e){ var ln = $(this).val().length; if(parseInt(ln) > 3){ printer_table.ajax.reload(); }else if(parseInt(ln) < 1){ printer_table.ajax.reload();} });
    $('#table-data').on('page.dt', function () {
        var info = printers.page.info();
        var limit_start = info.start;
        var limit_end = info.end;
        var length = info.length;
        var page = info.page;
        var pages = info.pages;
        // console.log( 'Showing page: '+info.page+' of '+info.pages);
        // console.log(limit_start,limit_end);
        $("#table-data-in").attr('data-limit-start',limit_start);
        $("#table-data-in").attr('data-limit-end',limit_end);
    });

    //CRUD
    $(document).on("click","#btn_save_printer",function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        var next =true;
        if($("#printer_type").val().length == 0){
            notif(0,'Tipe wajib diisi');
            $("#printer_type").focus();
            next=false;
        }else if($("#printer_name").val().length == 0){
            notif(0,'Nama wajib diisi');
            $("#printer_name").focus();
            next=false;
        }else{
            var form = new FormData($("#form-printer")[0]);
            // var form = new FormData();
            form.append('action', 'create');
            $.ajax({
                type: "POST",
                url: url,
                data: form, dataType:"json",
                cache: false, contentType: false, processData: false,
                beforeSend:function(){},
                success:function(d){
                    var s = d.status;
                    var m = d.message;
                    var r = d.result;
                    if(parseInt(s) == 1){
                        notif(s,m);
                        // formPrinterReset();
                        loadPrinterItem(r.id);
                        formPrinterItemSetDisplay(0);
                        $("#printer_id").val(r.id);
                        /* hint zz_for or zz_each */
                        printer_table.ajax.reload();
                    }else{
                        notif(s,m);
                    }
                },
                error:function(xhr,status,err){
                    notif(0,err);
                }
            });
        }
    });
    $(document).on("click",".btn_edit_printer",function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        var id       = $(this).attr('data-printer-id');
        var session  = $(this).attr('data-printer-session');
        var name     = $(this).attr('data-printer-name');

        var form = new FormData();
        form.append('action', 'read');
        form.append('printer_id', id);
        form.append('printer_session', session);
        form.append('printer_name', name);
        $.ajax({
            type: "post",
            url: url,
            data: form, dataType:"json",
            cache: false, contentType: false, processData: false,
            beforeSend:function(){},
            success:function(d){
                var s = d.status;
                var m = d.message;
                var r = d.result;
                if(parseInt(s)==1){ /* Success Message */
                    $("#div-form-printer").show(300);
                    
                    $("#printer_id").val(d.result.printer_id);
                    $("#printer_session").val(d.result.printer_session);
                    $("#printer_type").val(d.result.printer_type).trigger('change');
                    $("#printer_name").val(d.result.printer_name);
                    $("#printer_ip").val(d.result.printer_ip);                    
                    // $("#printer_note").val(d.result.printer_note);
                    $("#printer_flag").val(d.result.printer_flag).trigger('change');
                    // $("#printer_date_created").val(d.result.printer_date_created);

                    $("#btn_new_printer").hide();
                    $("#btn_save_printer").hide();
                    $("#btn_update_printer").show();
                    $("#btn_cancel_printer").show();

                    $("#btn_update_printer_item").hide(300);
                    $("#btn_save_printer_item").show(300);                    
                    // scrollUp('content');
                    formPrinterSetDisplay(0);
                    loadPrinterItem(r.printer_id);
                    formPrinterItemSetDisplay(0);
                }else{
                    $("#div-form-printer").hide(300);
                    notif(0,d.message);
                }
            },
            error:function(xhr, Status, err){
                notif(0,'Error');
            }
        });
    });
    $(document).on("click","#btn_update_printer",function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        var next =true;
        var id = $("#printer_id").val();
        var session = $("#printer_session").val();
        if(parseInt(id) > 0){
            if($("#printer_type").val().length == 0){
                notif(0,'printer_TYPE wajib diisi');
                $("#printer_type").focus();
                next=false;
            }else if($("#printer_name").val().length == 0){
                notif(0,'printer_NAME wajib diisi');
                $("#printer_name").focus();
                next=false;
            }else if($("#printer_flag").val().length == 0){
                notif(0,'printer_FLAG wajib diisi');
                $("#printer_flag").focus();
                next=false;
            }else{
                var form = new FormData($("#form-printer")[0]);
                form.append('action', 'update');
                form.append('printer_id', id);
                form.append('printer_session', session);
                $.ajax({
                    type: "POST",
                    url: url,
                    data: form,
                    dataType:"json",
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend:function(){},
                    success:function(d){
                        var s = d.status;
                        var m = d.message;
                        var r = d.result;
                        if(parseInt(s)==1){
                            // formReset();
                            notif(s,m);
                            printer_table.ajax.reload(null,false);
                        }else{
                            notif(s,m);  
                        }
                    },
                    error:function(xhr, Status, err){
                        notif(0,err);
                    }
                });
            }
        }else{
            notif(0,'Data belum dibuka');
        }
    });
    $(document).on("click",".btn_delete_printer",function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        var next     = true;
        var id       = $(this).attr('data-printer-id');
        var session  = $(this).attr('data-printer-session');
        var name     = $(this).attr('data-printer-name');

        $.confirm({
            title: 'Hapus!',
            content: 'Apakah anda ingin menghapus <b>'+name+'</b> ?',
            buttons: {
                confirm:{ 
                    btnClass: 'btn-danger',
                    text: 'Ya',
                    action: function () {
                        
                        var form = new FormData();
                        form.append('action', 'delete');
                        form.append('printer_id', id);
                        form.append('printer_session', session);
                        form.append('printer_name', name);
                        form.append('printer_flag', 4);

                        $.ajax({
                            type: "POST",
                            url : url,
                            data: form,
                            dataType:'json',
                            cache: false,
                            contentType: false,
                            processData: false,
                            success:function(d){
                                if(parseInt(d.status)==1){ 
                                    notif(s,d.message); 
                                    printer_table.ajax.reload(null,false);
                                }else{ 
                                    notif(s,d.message); 
                                }
                            }
                        });
                    }
                },
                cancel:{
                    btnClass: 'btn-success',
                    text: 'Batal', 
                    action: function () {
                        // $.alert('Canceled!');
                    }
                }
            }
        });
    });

    $(document).on("click",".btn_update_flag_printer",function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        var next     = true;
        var id       = $(this).attr('data-printer-id');
        var session  = $(this).attr('data-printer-session');
        var name     = $(this).attr('data-printer-name');
        var flag     = $(this).attr('data-printer-flag');

        if(parseInt(flag) == 0){
            var set_flag = 1;
            var msg = 'mengaktifkan';
        }else if(parseInt(flag) == 1){
            var set_flag = 0;
            var msg = 'menonaktifkan';
        }else{
            var set_flag = 4;
            var msg = 'menghapus';
        }

        $.confirm({
            title: 'Konfirmasi!',
            content: 'Apakah anda ingin '+msg+' <b>'+name+'</b> ?',
            buttons: {
                confirm:{ 
                    btnClass: 'btn-danger',
                    text: 'Ya',
                    action: function () {
                        
                        var form = new FormData();
                        form.append('action', 'update_flag');
                        form.append('printer_id', id);
                        form.append('printer_session', session);
                        form.append('printer_name', name);
                        form.append('printer_flag', set_flag);

                        $.ajax({
                            type: "POST",
                            url : url,
                            data: form,
                            dataType:'json',
                            cache: false,
                            contentType: false,
                            processData: false,
                            success:function(d){
                                if(parseInt(d.status)==1){ 
                                    notif(d.status,d.message); 
                                    printer_table.ajax.reload(null,false);
                                }else{ 
                                    notif(d.status,d.message); 
                                }
                            }
                        });
                    }
                },
                cancel:{
                    btnClass: 'btn-success',
                    text: 'Batal', 
                    action: function () {
                        // $.alert('Canceled!');
                    }
                }
            }
        });
    });

    //CRUD ITEM
    $(document).on("click","#btn_save_printer_item",function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        var id = $("#printer_id").val();
        var next = true;
        if(parseInt(id) > 0){
            if($("#printer_paper_width").val().length == 0){
                notif(0,'Lebar wajib diisi');
                $("#printer_paper_width").focus();
                next=false;
            }else if($("#printer_paper_height").val().length == 0){
                notif(0,'Panjang wajib diisi');
                $("#printer_paper_height").focus();
                next=false;
            }else{
                let form = new FormData($("#form-printer-item")[0]);
                form.append('action', 'create_printer_item');
                form.append('printer_parent_id',id);
                $.ajax({
                    type: "post",
                    url: url,
                    data: form, 
                    dataType: 'json', cache: 'false', 
                    contentType: false, processData: false,
                    beforeSend:function(){},
                    success:function(d){
                        let s = d.status;
                        let m = d.message;
                        let r = d.result;
                        if(parseInt(s) == 1){
                            notif(s,m);
                            /* hint zz_for or zz_each */
                            formPrinterItemReset();
                            loadPrinterItem(id);
                            formPrinterItemSetDisplay(0);
                        }else{
                            notif(s,m);
                        }
                    },
                    error:function(xhr,status,err){
                        notif(0,err);
                    }
                });
            }
        }else{
            notif('Silahkan refresh halaman ini');
        }
    });
    $(document).on("click",".btn_edit_printer_item",function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        var id = $(this).attr('data-id');
        if(parseInt(id) > 0){
            $.ajax({
                type: "post",
                url: url,
                data: {
                    action:'read_printer_item',
                    printer_id:id,
                }, 
                dataType: 'json', cache: 'false', 
                beforeSend:function(){},
                success:function(d){
                    let s = d.status;
                    let m = d.message;
                    let r = d.result;
                    if(parseInt(s) == 1){
                        formPrinterItemSetDisplay(0);                        
                        notif(s,m);
                        $("#id_printer_item").val(d.result.printer_id);
                        $("#printer_paper_design").val(d.result.printer_paper_design);
                        $("#printer_paper_width").val(d.result.printer_paper_width);
                        $("#printer_paper_height").val(d.result.printer_paper_height);      
                        // $('#printer_note').summernote('code', d.result.printer_note);
                        $("#btn_update_printer_item").show(300);
                        $("#btn_cancel_printer_item").show(300);                                          
                        $("#btn_save_printer_item").hide(300);
                    }else{
                        notif(s,m);
                    }
                },
                error:function(xhr,status,err){
                    notif(0,err);
                }
            });
        }else{
            notif(0,'ID tidak ditemukan'); 
        }
    });
    $(document).on("click","#btn_update_printer_item",function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        var printer_id = $("#printer_id").val();        
        var id = $("#id_printer_item").val();
        var next = true;
        if(parseInt(id) > 0){
            if($("#printer_paper_width").val().length == 0){
                notif(0,'Lebar wajib diisi');
                $("#printer_paper_width").focus();
                next=false;
            }else if($("#printer_paper_height").val().length == 0){
                notif(0,'Panjang wajib diisi');
                $("#printer_paper_height").focus();
                next=false;
            }else{            
                let form = new FormData($("#form-printer-item")[0]);
                form.append('action', 'update_printer_item');
                form.append('printer_id',id);            
                $.ajax({
                    type: "post",
                    url: url,
                    data: form, 
                    dataType: 'json', cache: 'false',
                    contentType: false, processData: false, 
                    beforeSend:function(){},
                    success:function(d){
                        let s = d.status;
                        let m = d.message;
                        let r = d.result;
                        if(parseInt(s) == 1){
                            notif(s,m);
                            formPrinterItemReset();
                            loadPrinterItem(printer_id);
                            formPrinterItemSetDisplay(0);
                        }else{
                            notif(s,m);
                        }
                    },
                    error:function(xhr,status,err){
                        notif(0,err);
                    }
                });
            }
        }else{
            notif(0,'ID tidak ditemukan'); 
        }
    });
    $(document).on("click",".btn_delete_printer_item",function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        var printer_id = $("#printer_id").val();          
        var id = $(this).attr('data-id');
        if(parseInt(id) > 0){      
            $.ajax({
                type: "post",
                url: url,
                data: {
                    action:'delete_printer_item',
                    printer_id:id
                }, 
                dataType: 'json', cache: 'false',
                // contentType: false, processData: false, 
                beforeSend:function(){},
                success:function(d){
                    let s = d.status;
                    let m = d.message;
                    let r = d.result;
                    if(parseInt(s) == 1){
                        notif(s,m);
                        loadPrinterItem(printer_id);
                    }else{
                        notif(s,m);
                    }
                },
                error:function(xhr,status,err){
                    notif(0,err);
                }
            });
        }else{
            notif(0,'ID tidak ditemukan'); 
        }        
    });

    //Additional
    $(document).on("click","#btn_new_printer",function(e) {
        formPrinterReset();
        formPrinterSetDisplay(0);         
        $("#div-form-printer").show(300);
        $("#btn_new_printer").hide(300);
    });
    $(document).on("click","#btn_cancel_printer",function(e) {
        formPrinterReset();
        $("#div-form-printer").hide(300); 
        $("#btn_new_printer").show(300);          
    });
    $(document).on("click",".btn_print_printer",function(e) {
        e.preventDefault();
        e.stopPropagation();
        console.log($(this));
        var id = $(this).attr('data-printer-id');
        var session = $(this).attr('data-printer-session');
        if(parseInt(id) > 0){
            var x = screen.width / 2 - 700 / 2;
            var y = screen.height / 2 - 450 / 2;
            var print_url = url_print+'?action=print&data='+session;
            var win = window.open(print_url,'Print','width=700,height=485,left=' + x + ',top=' + y + '').print();
        }else{
            notif(0,'Dokumen belum di buka');
        }
    });
    $(document).on("click","#btn_export_printer",function(e) {
        e.stopPropagation();
        $.alert('Fungsi belum dibuat');
    });
    $(document).on("click","#btn_print_printer",function(e) {
        e.preventDefault();
        e.stopPropagation();
        console.log($(this));
        // var id = $(this).attr('data-printer-id');
        $.alert('Fungsi belum dibuat');
    });
    $(document).on("click","#btn_cancel_printer_item",function(e) {
        formPrinterItemReset();
        formPrinterItemSetDisplay(0);  
    });
   

    function loadPrinterItem(printer_id = 0){
        if(parseInt(printer_id) > 0){
            $.ajax({
                type: "post",
                url: "<?= base_url('printer'); ?>",
                data: {
                    action:'load_printer_item_2',
                    printer_item_printer_id:printer_id
                },
                dataType: 'json', cache: 'false', 
                beforeSend:function(){},
                success:function(d){
                    let s = d.status;
                    let m = d.message;
                    let r = d.result;
                    if(parseInt(s) == 1){
                        let r = d.result;
                        let total_records = d.total_records;
                        if(parseInt(total_records) > 0){
                            $("#table_printer_item tbody").html('');
                        
                            let dsp = '';
                            for(let a=0; a < total_records; a++) {
                                let value = r[a];
                                dsp += '<tr>';
                                    // dsp += '<td>'+value['printer_paper_design']+'</td>';
                                    dsp += '<td style="text-align:right;">'+value['printer_paper_width']+'</td>';
                                    dsp += '<td style="text-align:right;">'+value['printer_paper_height']+'</td>';                                    
                                    dsp += '<td>';
                                        dsp += '<button type="button" class="btn-action btn btn-primary btn_edit_printer_item btn-mini btn-small" data-id="'+value['printer_id']+'" data-session="'+value['printer_session']+'">';
                                        dsp += 'Edit';
                                        dsp += '</button>';
                                        dsp += '<button type="button" class="btn-action btn btn-danger btn_delete_printer_item btn-mini btn-small" data-id="'+value['printer_id']+'" data-session="'+value['printer_session']+'">';
                                        dsp += 'Hapus';
                                        dsp += '</button>';                                        
                                    dsp += '</td>';
                                dsp += '</tr>';
                        
                            }
                            $("#table_printer_item tbody").html(dsp);
                        }
                    }else{
                        // notif(s,m);
                    }
                },
                error:function(xhr,status,err){
                    notif(0,err);
                }
            });
        }else{
            $("#table_printer_item tbody").html('');
        }
    }

}); //End of Document Ready

function imageLoad(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#upload1-src')
                .attr('src', e.target.result)
                .width(150)
                .height(150);
            $('.upload1-link')
            .attr('href', e.target.result);
            // $("#gambar-perbarui").removeClass('hide');
            // $("#upload1-hapus").removeClass('hide');
        };
        reader.readAsDataURL(input.files[0]);
    }
}
// window.setInterval(loadPlugin(),3000);
function loadPlugin(){
}
function formPrinterReset(){
    formPrinterSetDisplay(1);
    $("#form-printer input")
    .not("input[id='printer_date_start']")
    .not("input[id='printer_date_end']").val('');
    $("#form-printer textarea").val('');
    $("#table_printer_item tbody").html('');

    $("#btn_save_printer").show();
    $("#btn_update_printer").hide();
    $("#btn_cancel_printer").show();
    $("#div-form-printer").hide(300);    
}
function formPrinterItemReset(){
    formPrinterItemSetDisplay(1);    
    $("#form-printer-item input")
    .not("input[id='printer_item_date_start']")
    .not("input[id='printer_item_date_end']").val('');
    $("#form-printer-item textarea").val('');

    $("#btn_save_printer_item").show(300);
    $("#btn_update_printer_item").hide(300);
    $("#btn_cancel_printer_item").hide();  
}
function formPrinterSetDisplay(value){ // 1 = Untuk Enable/ ditampilkan, 0 = Disabled/ disembunyikan
    if(value == 1){ var flag = true; }else{ var flag = false; }
    //Attr Input yang perlu di setel
    var form = '#form-printer'; 
    var attrInput = [
       "printer_name","printer_ip"
    ];
    for (var i=0; i<=attrInput.length; i++) { $(""+ form +" input[name='"+attrInput[i]+"']").attr('readonly',flag); }

    //Attr Textarea yang perlu di setel
    var attrText = [
       "printer_note"
    ];
    for (var i=0; i<=attrText.length; i++) { $(""+ form +" textarea[name='"+attrText[i]+"']").attr('readonly',flag); }

    //Attr Select yang perlu di setel
    var atributSelect = [
       "printer_flag",
       "printer_type",
    ];
    for (var i=0; i<=atributSelect.length; i++) { $(""+ form +" select[name='"+atributSelect[i]+"']").attr('disabled',flag); }
}
function formPrinterItemSetDisplay(value){ // 1 = Untuk Enable/ ditampilkan, 0 = Disabled/ disembunyikan
    if(value == 1){ var flag = true; }else{ var flag = false; }
    //Attr Input yang perlu di setel
    var form = '#form-printer-item'; 
    var attrInput = [
       "printer_paper_name","printer_paper_design","printer_paper_height","printer_paper_width"
    ];
    for (var i=0; i<=attrInput.length; i++) { $(""+ form +" input[name='"+attrInput[i]+"']").attr('readonly',flag); }

    //Attr Textarea yang perlu di setel
    var attrText = [
    ];
    for (var i=0; i<=attrText.length; i++) { $(""+ form +" textarea[name='"+attrText[i]+"']").attr('readonly',flag); }

    //Attr Select yang perlu di setel
    var atributSelect = [
    ];
    for (var i=0; i<=atributSelect.length; i++) { $(""+ form +" select[name='"+atributSelect[i]+"']").attr('disabled',flag); }
}
</script>
