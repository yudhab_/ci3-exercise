<?php
$icon = 'none;';
// $icon = 'inline;';
?>
<div id="main-menu" class="page-sidebar col-md-2" style="padding-bottom: 30px!important;display: block!important;">
    <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper">
        <p class="menu-title sm" style="padding-top:0px!important;margin:0px 0px 0px!important;">
        </p>
        <ul id="sidebar" class="sidebarz">
            <li class="start"> 
                <a href="<?php echo base_url('admin'); ?>">
                    <i class="fas fa-home"></i>
                    <span class="title">Beranda</span> <span class="selected"></span>
                </a>
                <li class="start open">
                    <ul class="open sub-menu" style="display:block;">
                        <li><a href="<?php echo site_url('printer')?>"><i class="fas fa-hdd" style="display:<?php echo $icon; ?>"></i> Printer</a></li>
                        <li><a href="<?php echo site_url('voucher')?>"><i class="fas fa-archive" style="display:<?php echo $icon; ?>"></i> Voucher</a></li>                
                    </ul>
                </li>
            </li>           
        </ul>
        <ul id="sidebar" class="sidebarz">
            <li class="start"> 
                <a href="<?php echo base_url('admin'); ?>">
                    <i class="fas fa-home"></i>
                    <span class="title">Exercise</span> <span class="selected"></span>
                </a>
                <li class="start open">
                    <ul class="open sub-menu" style="display:block;">
                        <li><a href="<?php echo site_url('task1')?>"><i class="fas fa-hdd" style="display:<?php echo $icon; ?>"></i> Task 1</a></li>
                        <li><a href="<?php echo site_url('task2')?>"><i class="fas fa-archive" style="display:<?php echo $icon; ?>"></i> Task 2</a></li>
                        <li><a href="<?php echo site_url('task3')?>"><i class="fas fa-hdd" style="display:<?php echo $icon; ?>"></i> Task 3</a></li>
                        <li><a href="<?php echo site_url('task4')?>"><i class="fas fa-hdd" style="display:<?php echo $icon; ?>"></i> Task 4</a></li>                                                                
                    </ul>
                </li>
            </li>           
        </ul>        
        <div class="clearfix"></div>
        <br><br>
    </div>
</div>
<!-- <a href="#" class="scrollup">Scroll</a> -->

