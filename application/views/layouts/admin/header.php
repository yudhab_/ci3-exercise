<?php
$sidebar_logo = !empty($session['user_data']['branch']['branch_logo_sidebar']) ? site_url() . $session['user_data']['branch']['branch_logo_sidebar'] : site_url() . 'upload/branch/default_sidebar.png';
// var_dump($session['user_data']['branch']['branch_logo_sidebar']);die;
?>
<style>
    #logo-system{
        margin-top: 4px;
        margin-left: 4px;
        /*height: 28px;
        width: 132px;*/
        /*height: 34px;*/
        /*width: 134px;      */
        width: 200px;
    }
    #logo-system-2{
        margin-top: 1px;
        margin-left: 4px;
        /*height: 28px;
        width: 132px;*/
        /*height: 34px;*/
        /*width: 134px;*/
        width: 200px;
    }
    #horizontal-logo{
        position:relative;
        top:-8px;
        width: 200px;
    }    
    .notifcation-center{
        margin-top: 1px!important;
        margin-right: 1px!important;
    }
    .badge{
        padding-left: 4px!important;
        padding-right: 4px!important;
        bottom:5px!important;
        right: 70px!important;
        font-size: 12px!important;
    }
    @media (max-width: 767px){
        #logo-system {
            margin-top: 14px!important;
        }
    }

    .header .nav > li.quicklinks > a i{
        font-size: 14px!important;
    }
    .header .nav > li.quicklinks > a > span{
        font-size: 12px!important;
    }
    .dropdown-menu li{
        background-color: white;
        padding-left:8px;
        height: 30px;
    }
    .dropdown-menu li a{
        font-size: 14px!important;
    }
    .quicklinks a{
        padding:0px!important;
        /*font-size: 20px!important;*/
    }
    .ul-user-navigation{
        list-style:none;
        padding-left:0px;
    }
    .ul-user-navigation > li{
        padding-bottom:15px;
    }
    .ul-user-navigation > li > a:hover{
        cursor:pointer;
    }        
</style>

<div class="header navbar navbar-inverse" class="visible-xs visible-sm">
    <div class="navbar-inner" class="visible-xs visible-sm">
        <div class="header-seperation">
            <ul class="nav pull-left notifcation-center visible-xs visible-sm" style="">
                <li class="dropdown">
                    <a href="<?php echo base_url(); ?>" data-webarch="toggle-left-side">
                            <!-- <i class="material-icons">menu</i> -->
                        <i class="fa fa-bars"></i>
                    </a>
                </li>
            </ul>
            <a href="<?php echo base_url('admin'); ?>" class="">
                <img src="<?php echo $sidebar_logo; ?>"
                     class="logo" id="logo-system"/>
            </a>
            <ul style="height:40px!important;" class="nav pull-right notifcation-center">
                <li class="dropdown hidden hidden-xs hidden-sm">
                    <a href="<?php echo base_url(); ?>" class="dropdown-toggle active" data-toggle="">
                        <i class="material-icons">home</i>
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li class="dropdown visible-xs visible-sm">
                    <!-- <a href="<?php echo base_url('login/logout'); ?>"><i class="fa fa-power-off"></i></a> -->
                    <a href="#" class="btn-user-navigation" data-user="<?php echo ucfirst($session['user_data']['user_name']); ?>" data-is-r="<?php echo !empty($session['root']) ? $session['root'] : 0; ?>"><i class="fas fa-user-lock"></i></a>
                </li>
            </ul>
        </div>
        <div class="header-quick-nav visible-lg visible-md">
            <div class="col-md-7 pull-left padding-remove-left">
                <div class="col-md-12 col-xs-12">
                    <a href="#" class="">
                        <img src="<?php echo $sidebar_logo; ?>" class="hide logo" id="logo-system-2" data-src="<?php echo $sidebar_logo; ?>" data-src-retina="<?php echo $sidebar_logo; ?>"/>
                    </a>
                </div>
                <ul class="nav quick-section pull-left">
                </ul>
            </div>
            <div class="col-md-2 pull-right padding-remove-right">
                <ul class="nav quick-section pull-right">
                    <li class="quicklinks m-l-10 m-r-10">
                        <a data-toggle="dropdown" class="dropdown-toggle  pull-right " href="#" id="user-options" style="background-color: transparent!important;">
                            <i class="fa fa-user-lock"></i>
                            <span style="position: relative;">
                                &nbsp;
                                <?php echo ucfirst($session['user_data']['user_name']); ?>
                            </span>
                        </a>
                        <ul class="dropdown-menu pull-right" role="menu" aria-labelledby="user-options">                                       
                            <li>
                                <a href="<?= base_url('login/logout'); ?>" class="" onclick="<?= base_url('login/logout'); ?>">
                                    <i class="fa fa-power-off"></i><span style="position: relative;">&nbsp;Keluar</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div id="notification-list" style="display:none">
        <div style="width:300px">
        </div>
    </div>
</div>