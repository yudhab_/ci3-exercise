<?php
$project = ($_SERVER['SERVER_NAME']=='localhost') ? strtoupper(substr($_SERVER['PHP_SELF'],5,3)): 'Admin';

//Configuration User Menu Display From Session, 0=Vertical, 1=Horizontal
$user_menu_style = intval($session['menu_display']);
if($user_menu_style == 0){
	$body_class 				= '';
	$horizontal_menu_div_style  = 'display:none;';
	$horizontal_logo_style 		= 'display:none;';
}elseif($user_menu_style == 1){
	$body_class 				= 'horizontal-menu';
	$horizontal_menu_div_style 	= 'display:block;';
	$horizontal_logo_style 		= 'display:block;';
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

    <!-- <link rel="manifest" href="<?php #echo base_url();?>manifest.json"> -->
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/webarch/favicon.png" type="image/png">

    <!-- Mendeklarasikan warna yang muncul pada address bar Chrome versi seluler -->
    <meta name="theme-color" content="#fff" />

    <meta content="" name="description"/>
	<meta content="" name="author"/>
	
	<title><?php echo $project;?> : <?php echo $title; ?> : <?php echo ucfirst($session['user_data']['user_name']);?></title>
	<link href="<?php echo base_url();?>assets/webarch/favicon.png" sizes="16x16 32x32" type="image/png" rel="icon"> 

	<!-- Core CSS -->
	<link href="<?php echo base_url();?>assets/webarch/plugins/bootstrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url();?>assets/webarch/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url();?>assets/webarch/css/black.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url();?>assets/webarch/css/custom.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url();?>assets/webarch/css/webarch.css" rel="stylesheet" type="text/css"/>

	<!-- Icon & Notification -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/webarch/plugins/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css" />   
	<!-- <link href="<?php #echo base_url();?>assets/webarch/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/> -->
	<link href="<?php echo base_url();?>assets/webarch/plugins/jquery-notifications/css/messenger.css" rel="stylesheet" type="text/css" media="screen"/>
	<link href="<?php echo base_url();?>assets/webarch/plugins/jquery-notifications/css/messenger-theme-flat.css" rel="stylesheet" type="text/css" media="screen"/>  
	<link href="<?php echo base_url();?>assets/webarch/plugins/sweetalert2/sweetalert2.min.css"  rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url();?>assets/webarch/plugins/toastr/toastr.min.css">   

	<!-- Form, Confirm, Select, Image -->
	<link href="<?php echo base_url();?>assets/webarch/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url();?>assets/webarch/plugins/daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>assets/webarch/plugins/bootstrap-clockpicker/bootstrap-clockpicker.min.css" rel="stylesheet" type="text/css" media="screen"/>
	<link href="<?php echo base_url();?>assets/webarch/plugins/select2-4.0.8/css/select2.css" rel="stylesheet" type="text/css" media="screen"/>
	<link href="<?php echo base_url();?>assets/webarch/plugins/jconfirm-3.3.4/dist/jquery-confirm.min.css" rel="stylesheet">  
	<link href="<?php echo base_url();?>assets/webarch/plugins/croppie/css/croppie.css" rel="stylesheet" type="text/css"/>

	<!-- Datatable -->
	<link href="<?php echo base_url();?>assets/webarch/plugins/datatables-1.10.24/jquery.dataTables.css" rel="stylesheet" type="text/css"/>

	<!-- Other -->
	<link href="<?php echo base_url();?>assets/webarch/plugins/magnific-popup/magnific-popup.css" rel="stylesheet">

	<!-- Third Party -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.css" rel="stylesheet">
	  	
</head>
<body class="<?php echo $body_class; ?>"> <!-- horizontal-menu -->
	<?php include "header.php"; ?>
	<div class="page-container row">
		<?php 
			include "sidebar.php";                  
		?>
		<div id="page-content" class="page-content">
			<div id="portlet-config" class="modal">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body"> Widget settings form goes here 
				</div>
			</div>
			<div class="clearfix"></div>
			<div id="content" class="content col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<?php 
				if(isset($_view) && $_view)
					$this->load->view($_view);
				if(isset($_js_file) && $_js_file)
					$this->load->view($_js_file);
				?>                    
			</div>
		</div>
	  	<?php #include "chat.php";?>
	</div>     
	<!-- END CONTAINER -->

	<!-- Core JS -->
	<script src="<?php echo base_url();?>assets/webarch/plugins/pace/pace.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/webarch/plugins/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>  
	<script src="<?php echo base_url();?>assets/webarch/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/webarch/js/webarch.js" type="text/javascript"></script>	

	<!-- Icon & Notification -->
	<script src="<?php echo base_url();?>assets/webarch/plugins/jquery-notifications/js/messenger.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/webarch/plugins/sweetalert2/dist/sweetalert2.min.js"></script>

	<!-- Form,  Confirm, Select, Image -->
	<script src="<?php echo base_url();?>assets/webarch/plugins/autonumeric-4.1.0.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/webarch/plugins/daterangepicker/moment.min.js"></script>
	<script src="<?php echo base_url();?>assets/webarch/plugins/daterangepicker/daterangepicker.js"></script>	
	<script src="<?php echo base_url();?>assets/webarch/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/webarch/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/webarch/plugins/bootstrap-clockpicker/bootstrap-clockpicker.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/webarch/plugins/select2-4.0.8/js/select2.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/webarch/plugins/jconfirm-3.3.4/dist/jquery-confirm.min.js"></script>
	<script src="<?php echo base_url();?>assets/webarch/plugins/croppie/js/croppie.min.js"></script>

	<!-- Datatable -->
	<script src="<?php echo base_url();?>assets/webarch/plugins/datatables-1.10.24/jquery.dataTables.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/webarch/plugins/datatables-1.10.24/dataTables.rowGroup.js" type="text/javascript"></script>  

	<!-- Other -->   
	<script src="<?php echo base_url();?>assets/webarch/plugins/base64.js" type="text/javascript"></script>  
	<script src="<?php echo base_url();?>assets/webarch/plugins/jquery.redirect.js" type="text/javascript"></script>   
	<script src="<?php echo base_url();?>assets/webarch/plugins/magnific-popup/jquery.magnific-popup.js" type="text/javascript"></script> 

	<!-- Third Party -->    
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"></script>  
  	<script type="text/javascript">
		$(document).ready(function() {

		});

		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000,
			onOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		});
		function loader($stat) {
			if ($stat == 1) {
				swal({
					title: '<div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>',
					html: '<span style="font-size: 14px;">Loading...</span>',
					width: '20%',
					showConfirmButton: false,
					allowOutsideClick: false
				});
			} else if ($stat == 0) {
				swal.close();
			}
		}
		function notif($type,$msg) {
			if (parseInt($type) === 1) {
				//Toastr.success($msg);
				Toast.fire({
				type: 'success',
				title: $msg
				});
			} else if (parseInt($type) === 0) {
				//Toastr.error($msg);
				Toast.fire({
				type: 'error',
				title: $msg
				});
			}
		}   
		function activeTab(tab){
			$('.nav-tabs a[href="#' + tab + '"]').tab('show');
		}
		function approve(menu,id){
			window.open('<?= base_url();?>'+menu+'/prints/'+id)
		}
		function scrollUp(idelement){
			$([document.documentElement, document.body]).animate({
			scrollTop: $("#"+ idelement).offset().top
			}, 2000);
		}
		function addCommas(string){
			string += '';
			var x = string.split('.');
			var x1 = x[0];
			var x2 = x.length > 1 ? '.' + x[1] : '';
			var rgx = /(\d+)(\d{3})/;
			while (rgx.test(x1)) {
				x1 = x1.replace(rgx, '$1' + ',' + '$2');
			}
			return x1 + x2;
		}
		function removeCommas(string){

		 	return string.split(',').join("");
		}
  	</script>     
</body>
</html>